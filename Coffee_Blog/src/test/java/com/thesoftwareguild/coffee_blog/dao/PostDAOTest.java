///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.thesoftwareguild.coffee_blog.dao;
//
//import com.thesoftwareguild.coffee_blog.dto.Post;
//import com.thesoftwareguild.coffee_blog.dto.Tag;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//import org.springframework.jdbc.core.JdbcTemplate;
//
///**
// *
// * @author apprentice
// */
//public class PostDAOTest {
//
//    private PostDAO dao;
//
//    public PostDAOTest() {
//    }
//
//    @BeforeClass
//    public static void setUpClass() {
//    }
//
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    @Before
//    public void setUp() {
//        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:test-applicationContext.xml");
//        dao = (PostDAO) ctx.getBean("coffeeBlogDao");
//        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
//        cleaner.execute("delete from Posts");
//        cleaner.execute("delete from Tags");
//    }
//
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of addPost method, of class PostDAO.
//     */
//    @Test
//    public void testAddRemovePost() {
//        System.out.println("addPost, removePost");
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag("#YOLO"));
//        Post p = new Post("Title1", "Joe", "Blah Blah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        dao.addPost(p);
//        List<Post> posts = dao.getAllPosts();
//        assertEquals(p.getPostID(), posts.get(0).getPostID());
//        assertTrue(posts.get(0).getTags().get(0).getTag().equals("#YOLO"));
//        dao.removePost(posts.get(0).getPostID());
//        assertTrue(dao.getAllPosts().isEmpty());
//        assertFalse(!dao.getAllPosts().isEmpty());
//    }
//
//    /**
//     * Test of updatePost method, of class PostDAO.
//     */
//    @Test
//    public void testUpdateGetPostByIdPost() {
//        System.out.println("updatePost");
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag("#YOLO"));
//        Post p = new Post("Title1", "Joe", "Blah Blah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        dao.addPost(p);
//        List<Post> posts = dao.getAllPosts();
//        posts.get(0).setContent("Lul");
//        dao.updatePost(posts.get(0));
//        assertTrue(dao.getPostById(posts.get(0).getPostID()).getContent().equals("Lul"));
//        
//    }
//
//    /**
//     * Test of getAllPosts method, of class PostDAO.
//     */
//    @Test
//    public void testGetAllPosts() {
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag("#YOLO"));
//        Post p = new Post("Title1", "Joe", "Blah Blah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        Post q = new Post("Title2", "Bob", "Meh", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        dao.addPost(p);
//        dao.addPost(q);
//        
//        List<Post> posts = dao.getAllPosts();
//        assertEquals(2, posts.size());
//        assertTrue(posts.get(0).getTags().get(0).getTag().equals(posts.get(1).getTags().get(0).getTag()));
//    }
//
//    @Test
//    public void testGetVisibleGetUnapprovedPosts() {
//        System.out.println("getVisiblePosts");
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag("#YOLO"));
//        Post p = new Post("Title1", "Joe", "Blah Blah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2017-10-10"), tags);
//        Post q = new Post("Title2", "Bob", "Meh", false, false, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        Post r = new Post("Title3", "Bill", "Hoorah", false, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2017-10-10"), tags);
//        dao.addPost(p);
//        dao.addPost(q);
//        dao.addPost(r);
//        List<Post> unapprovedPosts = dao.getUnapprovedPosts();
//        List<Post> visiblePosts = dao.getVisiblePosts();
//        
//        assertEquals(2, unapprovedPosts.size());
//        assertEquals(2, visiblePosts.size());
//    }
//
//    /**
//     * Test of filterPosts method, of class PostDAO.
//     */
//    @Test
//    public void testFilterPosts() {
//        System.out.println("filterPosts");
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag("#Swag"));
//        List<Tag> tags1 = new ArrayList<>();
//        tags1.add(new Tag("#YOLO"));
//        Post p = new Post("Title1", "Joe", "Blah Blah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags1);
//        Post q = new Post("Title2", "Bob", "Meh", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags1);
//        Post r = new Post("Title3", "Bill", "Hoorah", true, true, LocalDate.parse("2012-12-12"), LocalDate.parse("2015-10-10"), tags);
//        dao.addPost(p);
//        dao.addPost(q);
//        dao.addPost(r);
//        List<Post> allPosts = dao.getAllPosts();
//        List<Post> filteredPosts = dao.filterPosts(allPosts.get(0).getTags().get(0).getTagID());
//        if(allPosts.get(0).getTags().get(0).getTag().equalsIgnoreCase("#Swag")){
//            assertEquals(1, filteredPosts.size());
//        }else{
//            assertEquals(2, filteredPosts.size());
//        }
//    }
//}
