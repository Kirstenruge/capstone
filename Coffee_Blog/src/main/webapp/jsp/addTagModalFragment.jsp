<%-- 
    Document   : addTagModalFragment
    Created on : Dec 4, 2016, 3:40:42 PM
    Author     : apprentice
--%>

<div class="modal fade" id="add-tags-modal" tabindex="-1" role="dialog"
     aria-labelledby="edit-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="edit-modal-label">Add Tags</h4>
            </div>
            <div class="modal-body">
                <h3 id="post-id"></h3>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="new-tag" id="tag-label" class="col-md-4 control-label hidden">
                            Add Tag:
                        </label>
                        <div class="col-md-offset-1 col-md-11" id="new-tag-div">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-11">
                            <button type="submit" id="new-tag-button" class="btn btn-default">
                                New Tag
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-11">
                            <button type="submit" id="add-tags-button" class="btn btn-default"
                                    data-dismiss="modal">
                                Add Tags
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">
                                Cancel
                            </button>
                            <input type="hidden" id="this-post-id">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> 
