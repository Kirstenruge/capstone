<%-- 
    Document   : login
    Created on : Nov 29, 2016, 2:44:08 PM
    Author     : apprentice
--%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="logo" href="${pageContext.request.contextPath}/img/coffeecup.jpg">

        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container-fluid">
                <nav class="navbar navbar-masthead navbar-fixed-top navbar-custom">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="home"><img src="img/coffeecup.jpg" alt="Logo"></a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>

                            </ul>


                        </div>  </div>
                </nav>
                <div class="text-center">
                    <h2>Beanz Coffee Forum Login</h2>
                    <!-- #1 - If login_error == 1 then there was a failed login attempt -->
                    <!-- so display an error message -->
                    <c:if test="${param.login_error == 1}">
                        <h3>Wrong id or password!</h3>
                    </c:if>
                    <!-- #2 - Post to Spring security to check our authentication -->
                    <form method="post" class="signin" action="j_spring_security_check">
                        <fieldset>
                            <label for="username">Username
                            </label>
                            <input id="username_or_email"
                                   name="j_username"
                                   type="text" /><br>
                            <label for="password">Password</label>
                            <input id="password"
                                   name="j_password"
                                   type="password" /><br>
                            <input class="btn" name="commit" type="submit" value="Sign In" />
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>