<%-- 
    Document   : pages
    Created on : Dec 2, 2016, 10:08:41 AM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container-fluid">
                <nav class="navbar navbar-masthead navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Beanz Coffee Logo</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="${pageContext.request.contextPath}/mainAdmin">Admin Portal</a></li>

                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="${pageContext.request.contextPath}/login">Log In</a></li>
                        </ul>
                    </div>
                </nav>
                <div class="row">
                    <div class="row">
                        <div class="col-sm-7 col-sm-offset-1 blog-main">

                            <h2>  <c:out value="${title}"></c:out> </h2><hr>
                            <div>
                               
                                <br>
                                
                                <c:out value="${content}"> </c:out>
                             
                                
                            </div>
                       

                            <h2 class="text-center"> Beanz Coffee </h2><hr>
                            <div id="log"></div>
                            <div id="contents"></div>

                        </div>
                       <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
                            <div class="sidebar-module sidebar-module-inset">
                                <h4>About</h4>
                                <p>class makeCoffee(){<br><em>boolean steaming = true;<br></em> boolean delicious = true; <br>}</p>
                            </div>
                            <div class="sidebar-module">
                                Links
                                <div id="log"> </div>
                              
                               
                            </div>
                            <div class="sidebar-module">
                                <h4>Elsewhere</h4>
                                <ol class="list-unstyled">
                                    <li><a href="#">Instagram</a></li>
                                    <li><a href="#">Twitter</a></li>
                                    <li><a href="#">Facebook</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
       
        <script src="${pageContext.request.contextPath}/js/page.js"></script>
    </body>
</html>


