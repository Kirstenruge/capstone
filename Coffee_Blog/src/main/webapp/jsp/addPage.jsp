<%-- 
    Document   : addPost
    Created on : Nov 29, 2016, 3:35:48 PM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="logo" href="${pageContext.request.contextPath}/img/coffeecup.jpg">

        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container-fluid">
                <nav class="navbar navbar-masthead navbar-fixed-top navbar-custom">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="home"><img src="img/coffeecup.jpg" alt="Logo"></a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>
                                    <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                    <li class="active"><a href="${pageContext.request.contextPath}/mainAdmin">Admin Portal</a></li>
                                    </sec:authorize>
                            </ul>
                            <sec:authorize access="!hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="${pageContext.request.contextPath}/login">Log In</a></li>
                                </ul>
                            </sec:authorize>
                            <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a></li>
                                </ul>
                            </sec:authorize>
                        </div>  </div>
                </nav>
                <div class="row">
                    <h2 class="text-center">Welcome, Admin!</h2><hr>
                    <div class="col-sm-8 blog-main">
                        <form class="form-horizontal" action="addNewPage" method="POST" role="form">
                            <div class =" textBox" style="left-align">
                                <label for="title" class="control-label">Add Title: </label>
                                <input type="text" id="title" name="title" class = "form-control">
                            </div><br>
                            <textarea id="content" name="postContent" placeholder="Add Content Here"></textarea><br>
                            <div class="form-group"><center>
                                <button id="save-button" name="submit" type="submit" class="btn btn-default">
                                    Submit
                                </button></center>
                            </div>
                        </form>
                        
                    </div>
                             <div class="col-sm-3 col-sm-offset-1 blog-sidebar text-center">
                        What would you like to do?<br>
                        <a href="${pageContext.request.contextPath}/addPost"> Add a Blog Post</a><br>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <a id="approve_posts_link" href="${pageContext.request.contextPath}/approvePosts">Approve Posts</a><br>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">    
                            <a href="${pageContext.request.contextPath}/addPage"> Add a New Page</a><br>
                        </sec:authorize>
                        <sec:authorize  access="hasRole('ROLE_ADMIN')">
                            <a href="${pageContext.request.contextPath}/editPages">Edit Pages</a><br>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <a href="${pageContext.request.contextPath}/displayUserList">Edit Users</a><br>
                        </sec:authorize>
                    </div>

                </div>
            </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/admin2.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

        <script>tinymce.init({
                selector: 'textarea',
                height: 500,
                theme: 'modern',
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        </script>
    </body>
</html>
