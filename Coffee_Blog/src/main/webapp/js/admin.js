var toBeApproved = 0;

$(document).ready(function () {
    loadAllPosts();

    $('#new-tag-button').click(function (event) {
        var empty = false;
        event.preventDefault();
        $('input[name="new-tags"]').each(function () {
            if ($(this).val() === "") {
                empty = true;
            }
        });
        if (!empty) {
            $('#new-tag-div').append('<div><input name="new-tags" type="text" placeholder="New Tag" class="form-control"><a href="#" class="remove-field"><span class="glyphicon glyphicon-remove"></span></a></input></div>');
        }
    });

    $('#add-tags-button').click(function (event) {
        event.preventDefault();
        $('input[name="new-tags"]').each(function () {
            if ($(this).val() !== "") {

                $.ajax({
                    type: 'PUT',
                    url: 'addTag/' + $('#this-post-id').val(),
                    data: JSON.stringify($(this).val()),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    'dataType': 'json'
                }).success(function () {
                    loadAllPosts();
                });
            }
        });
    });
    $('#new-tag-div').on("click", ".remove-field", function (event) {
        event.preventDefault();
        $(this).parent('div').remove();
    });
});

function loadAllPosts() {
    $.ajax({
        url: "allPosts"
    }).success(function (data, status) {
        fillDiv(data, status);
    });
}

function fillDiv(postList, status) {
    var div = $("#admin-all-posts");
    div.empty();
    $.each(postList, function (index, post) {
        title = post.title;
        str = post.content;
        date = post.creationDateString;
        postId = post.postID;
        postDate = post.postDateString;
        if(postDate === "1900-01-01"){
            postDate = "N/A";
        }
        expirationDate = post.expirationDateString;
        if(expirationDate === "2999-12-12"){
            expirationDate = "N/A";
        }
        visible = post.visible;
        if (visible === true){
            visible = "Yes";
        }else{
            visible = "No";
        }
        approved = post.approved;
        author = post.username;
        html = $.parseHTML(str),
                nodeNames = [];
        tags = post.tags;

        div.append($('<div class=well>').append(title));
        div.append($('</div>'));
        div.append($('<div>').append(html));
        div.append($('<br>'));
        div.append('Created On: ' + date);
        div.append($('<br>'));
        div.append('Post Date: ' + postDate);
        div.append($('<br>'));
        div.append('Expiration Date: ' + expirationDate);
        div.append($('<br>'));
        div.append('Visible: ' + visible);
        div.append($('<br>'));
        $.each(tags, function (index, tag) {
            div.append(tag.tag + " | ");
        });
        div.append($('<br>'));
        div.append($('<button class="btn btn-default btn-sm"><a>')
                .attr({
                    'data-post-id': postId,
                    'data-toggle': 'modal',
                    'data-target': '#add-tags-modal'
                }).text("Add Tags"));
        div.append($('<br>'));
        div.append("Posted by: " + author);
        div.append($('<br>'));
        div.append($('<a>').attr("href", "editPost/" + postId).append($('<button type="submit" class="btn btn-default btn-sm">').text("Edit")));
        div.append(" | ");
        div.append($('<a>').attr("href", "deletePost/" + postId).append($('<button class="btn btn-danger btn-sm">').text("Delete")));
        if (approved === false) {
            div.append(" | ");
            div.append($('<a>').attr("href", "approvePost/" + postId).append($('<button class="btn btn-sm btn-primary"><a>').text("Approve Post")));
            toBeApproved++;
        }
        div.append($('<hr>'));


// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
    if (toBeApproved > 0) {
        $("#approve_posts_link").append("(" + toBeApproved + ")");
    }
}

$('#add-tags-modal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var postId = element.data('post-id');
    var modal = $(this);
    modal.find('#this-post-id').val(postId);
});