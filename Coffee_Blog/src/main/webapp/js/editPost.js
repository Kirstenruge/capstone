/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var counter = 0;
var limit = 10;
$(document).ready(function () {
    loadTags();
    $('#new-tag-button').click(function (event) {
        event.preventDefault();
        if (counter === limit) {
            alert("You have reached the limit of adding " + counter + " tags.");
        } else {
            $('#tag-input-form').append('<div class="col-md-6"><input name="new-tags" type="text" id="tags" placeholder="New Tag" class="form-control"><a href="#" class="remove-field"><span class="glyphicon glyphicon-remove"></span></a></input></div>');
            counter++;
        }
    });
    $('#tag-input-form').on("click", ".remove-field", function (event) {
        event.preventDefault();
        $(this).parent('div').remove();
        $(this).parent.empty();
    });
});

function loadTags(){
    var postID = $('#postID').val();
    $.ajax({
        url: "../getTags/" + postID 
    }).success(function (data, status) {
        fillTagForm(data, status);
    });

}

function fillTagForm(tagList, status){
    var div = $('#tag-input-form');
    $.each(tagList, function(index, tag){
       tag = tag.tag;
       tagId = tag.tagID;
       div.append('<div class="col-md-6"><input name="new-tags" id='+tagId+' type="text" value='+tag+' class="form-control"><a href="#" class="remove-field"><span class="glyphicon glyphicon-remove"></span></a></input></div>');
    });
}