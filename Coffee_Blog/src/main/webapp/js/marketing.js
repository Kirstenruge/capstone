var counter = 0;
var limit = 10;
$(document).ready(function () {
    loadAllPosts();

    $('#new-tag-button').click(function (event) {
        event.preventDefault();
        if (counter === limit) {
            alert("You have reached the limit of adding " + counter + " tags.");
        } else {
            $('#new-tag-div').append('<div class="col-md-6"><input name="new-tags" type="text" id="tags" placeholder="New Tag" class="form-control"><a href="#" class="remove-field"><span class="glyphicon glyphicon-remove"></span></a></input></div>');
            counter++;
        }
    });
    $('#new-tag-div').on("click", ".remove-field", function (event) {
        event.preventDefault();
        $(this).parent('div').remove();
        $(this).parent.empty();
        counter--;
    });

    $('#add-tags-button').click(function (event) {
        event.preventDefault();
        $('input[name="new-tags"]').each(function () {
            if ($(this).val() !== "") {

                $.ajax({
                    type: 'PUT',
                    url: 'addTag/' + $('#this-post-id').val(),
                    data: JSON.stringify($(this).val()),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    'dataType': 'json'
                }).success(function () {
                    loadAllPosts();
                });
            }
        });
    });

});

function loadAllPosts() {
    $.ajax({
        url: "allPosts"
    }).success(function (data, status) {
        fillDiv(data, status);
    });
}

function fillDiv(postList, status) {
        var div = $("#admin-all-posts");
    div.empty();
    $.each(postList, function (index, post) {
        title = post.title;
        str = post.content;
        date = post.creationDateString;
        postId = post.postID;
        postDate = post.postDateString;
        if(postDate === "1900-01-01"){
            postDate = "N/A";
        }
        expirationDate = post.expirationDateString;
        if(expirationDate === "2999-12-12"){
            expirationDate = "N/A";
        }
        visible = post.visible;
        if (visible === true){
            visible = "Yes";
        }else{
            visible = "No";
        }
        approved = post.approved;
        author = post.username;
        html = $.parseHTML(str),
                nodeNames = [];
        tags = post.tags;

        div.append($('<div class=well>').append(title));
        div.append($('</div>'));
        div.append($('<div>').append(html));
        div.append($('<br>'));
        div.append('Created On: ' + date);
        div.append($('<br>'));
        div.append('Post Date: ' + postDate);
        div.append($('<br>'));
        div.append('Expiration Date: ' + expirationDate);
        div.append($('<br>'));
        div.append('Visible: ' + visible);
        div.append($('<br>'));
        $.each(tags, function (index, tag) {
            div.append(tag.tag + " | ");
        });
        div.append($('<br>'));
        div.append($('<button class="btn btn-default btn-sm"><a>')
                .attr({
                    'data-post-id': postId,
                    'data-toggle': 'modal',
                    'data-target': '#add-tags-modal'
                }).text("Add Tags"));
        div.append($('<br>'));
        div.append("Posted by: " + author);
        div.append($('<br>'));
        div.append($('<hr>'));


// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
}

$('#add-tags-modal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var postId = element.data('post-id');
    var modal = $(this);
    modal.find('#this-post-id').val(postId);
});
