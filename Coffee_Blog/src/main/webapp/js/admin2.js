/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var counter = 0;
var limit = 10;
$(document).ready(function () {
    loadUnapprovedPosts();
    $('#new-tag-button').click(function (event) {
        event.preventDefault();
        if (counter === limit) {
            alert("You have reached the limit of adding " + counter + " tags.");
        } else {
            $('#new-tag-div').append('<div class="col-md-6"><input name="new-tags" type="text" id="tags" placeholder="New Tag" class="form-control"><a href="#" class="remove-field"><span class="glyphicon glyphicon-remove"></span></a></input></div>');
            counter++;
        }
    });
    $('#new-tag-div').on("click", ".remove-field", function (event) {
        event.preventDefault();
        $(this).parent('div').remove();
        $(this).parent.empty();
        counter--;
    });
});

function loadUnapprovedPosts() {
    $.ajax({
        url: "unapprovedPosts"
    }).success(function (data, status) {
        fillDiv(data, status);
    });
}

function fillDiv(postList, status) {
    var div = $("#postsDiv");
    $.each(postList, function (index, post) {
        title = post.title;
        str = post.content;
        date = post.creationDateString;
        postId = post.postID;
        postDate = post.postDateString;
        expirationDate = post.expirationDateString;
        visible = post.visible;
        approved = post.approved;
        author = post.username;
        html = $.parseHTML(str),
                nodeNames = [];
        tags = post.tags;

        div.append($('<div class=well>').append(title));
        div.append($('</div>'));
        div.append($('<div>').append(html));
        div.append($('<br>'));
        div.append('Created On: ' + date);
        div.append($('<br>'));
        div.append('Post Date: ' + postDate);
        div.append($('<br>'));
        div.append('Expiration Date: ' + expirationDate);
        div.append($('<br>'));
        div.append('Visible: ' + visible);
        div.append($('<br>'));
        $.each(tags, function (index, tag) {
            div.append(tag.tag + " | ");
        });
       
        div.append($('<br>'));
        div.append("Posted by: " + author);
        div.append($('<br>'));
        div.append($('<a>').attr("href", "editPost/" + postId).text("Edit"));
        if (approved === false) {
            div.append(" | ");
            div.append($('<a>').attr("href", "approvePost/" + postId).text("Approve Post"));
        }
        div.append($('<hr>'));


// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });

}