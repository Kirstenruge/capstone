/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dao;

import com.thesoftwareguild.coffee_blog.dto.User;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface UserDao {

    public User addUser(User newUser);

    public void deleteUser(String username);

    public List<User> getAllUsers();
    
    public void changePassword(String username,String newPassword);
}

