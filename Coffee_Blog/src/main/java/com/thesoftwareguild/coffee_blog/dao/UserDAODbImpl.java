/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dao;

import com.thesoftwareguild.coffee_blog.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class UserDAODbImpl implements UserDao {

    private static final String SQL_INSERT_USER
            = "insert into Users (username, password, enabled) values (?, ?, 1)";
    private static final String SQL_INSERT_AUTHORITY
            = "insert into Authorities (username, authority) values (?, ?)";
    private static final String SQL_DELETE_USER
            = "delete from Users where username = ?";
    private static final String SQL_DELETE_AUTHORITIES
            = "delete from Authorities where username = ?";
    private static final String SQL_GET_ALL_USERS
            = "select username, password from Users";
    private static final String SQL_GET_ALL_AUTHORITIES
            = "select authority from Authorities where username = ?";
    private static final String SQL_GET_USER
            = "select username, password from Users where username = ?";
    private static final String SQL_SET_PASSWORD
            = "update Users set password = ? where username = ?";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public User addUser(User newUser) {
        List<User> users = new ArrayList<>();
        users = jdbcTemplate.query(SQL_GET_ALL_USERS, new UserMapper());

        for (User u : users) {
            if (u.getUsername().equals(newUser.getUsername())) {
                return null;
            }
        }

        jdbcTemplate.update(SQL_INSERT_USER, newUser.getUsername(), newUser.getPassword());

        // now insert user's roles
        List<String> authorities = newUser.getAuthorities();
        for (String authority : authorities) {
            jdbcTemplate.update(SQL_INSERT_AUTHORITY, newUser.getUsername(), authority);
        }

        return newUser;
    }

    @Override
    public void deleteUser(String username) {
        // #3 - First delete all authorities for this user
        jdbcTemplate.update(SQL_DELETE_AUTHORITIES, username);
        // #3 - Second delete the user - failing to do so will result in foreign
        // key constraint errors
        jdbcTemplate.update(SQL_DELETE_USER, username);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        users = jdbcTemplate.query(SQL_GET_ALL_USERS, new UserMapper());
        for (User u : users) {
            u.setAuthorities(jdbcTemplate.query(SQL_GET_ALL_AUTHORITIES, new AuthorityMapper(), u.getUsername()));
        }
        return users;
    }

    @Override
    public void changePassword(String username, String newPassword) {

        jdbcTemplate.update(SQL_SET_PASSWORD, newPassword, username);

    }

    private class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int i) throws SQLException {
            User u = new User();
            u.setUsername(rs.getString("username"));
            u.setPassword(rs.getString("password"));
            return u;
        }
    }

    private class AuthorityMapper implements RowMapper<String> {

        public String mapRow(ResultSet rs, int i) throws SQLException {
            return rs.getString("authority");
        }
    }

}
