/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dao;

import com.thesoftwareguild.coffee_blog.dto.Page;
import com.thesoftwareguild.coffee_blog.dto.Post;
import com.thesoftwareguild.coffee_blog.dto.Tag;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface PostDAO {
   public Post addPost(Post post);
    // remove the Post with the given id from the data store

    /**
     *
     * @param postId
     * @param tag
     * @return
     */
   
   public Tag addTag(Tag tag); 
   public void removePost(int postId);
    // update the given Post in the data store

    public void updatePost(Post post);
    // retrieve all Posts from the data store

    public List<Post> getAllPosts();
    // retrieve the Post with the given id from the data store

    public Post getPostById(int postId);
    // search for Posts by the given search criteria values

    public List<Post> getVisiblePosts();
    
    public List<Post> getUnapprovedPosts();
   
    public List<Post> filterPosts(int tagId);

    public void updateVisibility();
    
    public Page addPage(Page page);
    
    public List<Page> getAllPages();
   
    public Page getPageById(int pageId);

    public void removePage(int id);
    
    public void updatePage(Page page);
    
    public List<Tag> getTagsById(int id);
}