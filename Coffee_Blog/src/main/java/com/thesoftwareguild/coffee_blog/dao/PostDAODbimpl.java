/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dao;

import com.thesoftwareguild.coffee_blog.dto.Page;
import com.thesoftwareguild.coffee_blog.dto.Post;
import com.thesoftwareguild.coffee_blog.dto.Tag;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class PostDAODbimpl implements PostDAO {

    private static final String SQL_INSERT_POST
            = "insert into Posts (creation_date, title, username, content, approved, visible, post_date, expiration_date) values (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_INSERT_TAG
            = "insert into Tags (tag, post_id) values (?, ?)";
    private static final String SQL_DELETE_POST
            = "delete from Posts where post_id = ?";
    private static final String SQL_DELETE_TAGS
            = "delete from Tags where post_id = ?";
    private static final String SQL_SELECT_POST
            = "select * from Posts where post_id = ?";
    private static final String SQL_UPDATE_POST
            = "update Posts set title = ?, content = ?, post_date = ?, expiration_date = ?, visible = ?, approved = ? where post_id = ?";
    private static final String SQL_UPDATE_TAG
            = "update Tags set tag = ? where tag_id = ?";
    private static final String SQL_DELETE_TAG
            = "delete from Tags where tag_id = ?";
    private static final String SQL_SELECT_ALL_POSTS
            = "select * from Posts";
    private static final String SQL_SELECT_TAGS_BY_POST
            = "SELECT * FROM Tags INNER JOIN Posts ON Tags.post_id = Posts.post_id WHERE Posts.post_id = ?";
    private static final String SQL_SELECT_TAGS_BY_CONTENT
            = "SELECT * FROM Tags WHERE tag = ? AND post_id = ?";
    private static final String SQL_SELECT_POSTS_BY_USERNAME
            = "select * from Posts where username = ?";
    private static final String SQL_SELECT_UNAPPROVED_POSTS
            = "select * from Posts where approved = 0";
    private static final String SQL_SELECT_VISIBLE_POSTS
            = "select * from Posts where visible = 1";
    private static final String SQL_SEARCH_POSTS_BY_TAGID
            = "SELECT * FROM Posts INNER JOIN Tags ON Tags.post_id = Posts.post_id "
            + "WHERE Tags.tag_id = ?";
    private static final String SQL_SEARCH_POSTS_BY_TAG
            = "SELECT * FROM Posts INNER JOIN Tags ON Tags.post_id = Posts.post_id "
            + "WHERE Tags.tag = ?";
    private static final String SQL_UPDATE_POST_VISIBILITY
            = "UPDATE Posts\n"
            + "SET Posts.visible = 0\n"
            + "WHERE NOT CURDATE() BETWEEN Posts.post_date AND Posts.expiration_date\n"
            + "AND Posts.approved = 1";
    private static final String SQL_UPDATE_POST_VISIBILITY_2
            = "UPDATE Posts\n"
            + "Set Posts.visible = 1\n"
            + "WHERE CURDATE() BETWEEN Posts.post_date AND Posts.expiration_date\n"
            + "AND Posts.approved = 1";
    private static final String SQL_INSERT_PAGE
            = "insert into Pages (content, title) values (?, ?)";
    private static final String SQL_SELECT_ALL_PAGES
            = "select * from Pages";
    private static final String SQL_SELECT_PAGE
            = "select * from Pages where page_id = ?";
    private static final String SQL_DELETE_PAGE
            = "delete from Pages where page_id = ?";
    private static final String SQL_UPDATE_PAGE
            = "update Pages set title = ?, content = ? where page_id = ?";
    private static JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Post addPost(Post post) {
        Date creationDate = Date.valueOf(post.getCreationDate());
        Date postDate = Date.valueOf(post.getPostDate());
        Date expirationDate = Date.valueOf(post.getExpirationDate());
        jdbcTemplate.update(SQL_INSERT_POST,
                creationDate,
                post.getTitle(),
                post.getUsername(),
                post.getContent(),
                post.isApproved(),
                post.isVisible(),
                postDate,
                expirationDate);
        post.setPostID(jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class));
        for (Tag t : post.getTags()) {
            jdbcTemplate.update(SQL_INSERT_TAG,
                    t.getTag(),
                    post.getPostID());
            t.setTagID(jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                    Integer.class));
        }
        return post;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Page addPage(Page page) {
        String content = page.getContent();
        String title = page.getTitle();

        jdbcTemplate.update(SQL_INSERT_PAGE,
                page.getContent(),
                page.getTitle());

        page.setPageID(jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class));

        return page;
    }

    @Override
    public void removePost(int postId) {
        jdbcTemplate.update(SQL_DELETE_POST, postId);
        jdbcTemplate.update(SQL_DELETE_TAGS, postId);
    }

    @Override
    public void updatePost(Post post) {
        Date postDate = Date.valueOf(post.getPostDate());
        Date expirationDate = Date.valueOf(post.getExpirationDate());
        jdbcTemplate.update(SQL_UPDATE_POST,
                post.getTitle(),
                post.getContent(),
                postDate,
                expirationDate,
                post.isVisible(),
                post.isApproved(),
                post.getPostID());
        jdbcTemplate.update(SQL_DELETE_TAGS, post.getPostID());
        if (post.getTags() != null) {
            for (Tag t : post.getTags()) {
                jdbcTemplate.update(SQL_INSERT_TAG,
                        t.getTag(),
                        post.getPostID());
                t.setTagID(jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class));
            }
        }
    }

    @Override
    public List<Post> getAllPosts() {
        updateVisibility();
        List<Post> postList = jdbcTemplate.query(SQL_SELECT_ALL_POSTS, new PostMapper());
        postList.sort((o1, o2) -> o2.getPostDate().compareTo(o1.getPostDate()));
        return postList;
    }

    @Override
    public List<Page> getAllPages() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PAGES, new PageMapper());

    }

    @Override
    public Post getPostById(int postId) {
        try {
            Post p = jdbcTemplate.queryForObject(SQL_SELECT_POST, new PostMapper(), postId);
            return p;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Page getPageById(int pageId) {
        try {
            Page p = jdbcTemplate.queryForObject(SQL_SELECT_PAGE, new PageMapper(), pageId);
            return p;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Post> getVisiblePosts() {
        updateVisibility();
        List<Post> postList = jdbcTemplate.query(SQL_SELECT_VISIBLE_POSTS, new PostMapper());
        postList.sort((o1, o2) -> o2.getPostDate().compareTo(o1.getPostDate()));
        return postList;
    }

    @Override
    public List<Post> getUnapprovedPosts() {
        return jdbcTemplate.query(SQL_SELECT_UNAPPROVED_POSTS, new PostMapper());
    }

    @Override
    public List<Post> filterPosts(int tagId) {
        List<Post> postList = jdbcTemplate.query(SQL_SEARCH_POSTS_BY_TAGID, new PostMapper(), tagId);
        String tagContent = postList.get(0).getTags().get(0).getTag();
        for (Tag t : postList.get(0).getTags()) {
            if (t.getTagID() == tagId) {
                tagContent = t.getTag();
            }
        }
        postList = jdbcTemplate.query(SQL_SEARCH_POSTS_BY_TAG, new PostMapper(), tagContent);
        postList.sort((o1, o2) -> o2.getPostDate().compareTo(o1.getPostDate()));
        return postList;
    }

    @Override
    public Tag addTag(Tag tag) {
        jdbcTemplate.update(SQL_INSERT_TAG,
                tag.getTag(),
                tag.getPostID());
        return tag;
    }

    @Override
    public void updateVisibility() {
        jdbcTemplate.update(SQL_UPDATE_POST_VISIBILITY);
        jdbcTemplate.update(SQL_UPDATE_POST_VISIBILITY_2);
    }

    @Override
    public void removePage(int id) {
        jdbcTemplate.update(SQL_DELETE_PAGE, id);
    }

    @Override
    public void updatePage(Page page) {
        jdbcTemplate.update(SQL_UPDATE_PAGE,
                page.getTitle(),
                page.getContent(),
                page.getPageID());
    }

    @Override
    public List<Tag> getTagsById(int id) {
        return jdbcTemplate.query(SQL_SELECT_TAGS_BY_POST, new TagMapper(), id);
    }

    private static final class PostMapper implements RowMapper<Post> {

        @Override
        public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
            Post post = new Post();
            post.setPostID(rs.getInt("post_id"));
            post.setTitle(rs.getString("title"));
            post.setContent(rs.getString("content"));
            post.setCreationDate(rs.getDate("creation_date").toLocalDate());
            post.setUsername(rs.getString("username"));
            post.setApproved(rs.getBoolean("approved"));
            post.setVisible(rs.getBoolean("visible"));
            post.setPostDate(rs.getDate("post_date").toLocalDate());
            post.setExpirationDate(rs.getDate("expiration_date").toLocalDate());
            post.setTags(jdbcTemplate.query(SQL_SELECT_TAGS_BY_POST, new TagMapper(), post.getPostID()));
            return post;

        }
    }

    private static final class PageMapper implements RowMapper<Page> {

        @Override
        public Page mapRow(ResultSet rs, int rowNum) throws SQLException {
            Page page = new Page();
            page.setTitle(rs.getString("title"));
            page.setPageID(rs.getInt("page_id"));
            page.setContent(rs.getString("content"));
            return page;
        }
    }

    private static final class TagMapper implements RowMapper<Tag> {

        @Override
        public Tag mapRow(ResultSet rs, int rowNum) throws SQLException {
            Tag tag = new Tag();
            tag.setTag(rs.getString("tag"));
            tag.setTagID(rs.getInt("tag_id"));
            return tag;
        }
    }
}
