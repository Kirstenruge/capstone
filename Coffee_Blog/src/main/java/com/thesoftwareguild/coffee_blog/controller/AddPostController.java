/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.controller;

import com.thesoftwareguild.coffee_blog.dao.PostDAO;
import com.thesoftwareguild.coffee_blog.dto.Page;
import com.thesoftwareguild.coffee_blog.dto.Post;
import com.thesoftwareguild.coffee_blog.dto.Tag;
import com.thesoftwareguild.coffee_blog.dto.Wrapper;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class AddPostController {

    private PostDAO dao;

    @Inject
    public AddPostController(PostDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/mainAdmin"}, method = RequestMethod.GET)
    public String displayMainPage() {

        return "adminPortal";
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Post getPost(@PathVariable("id") int id) {
        // Retrieve the Post associated with the given id and return it
        return dao.getPostById(id);
    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePost(@PathVariable("id") int id) {
        dao.removePost(id);
    }

    @RequestMapping(value = "/addNewPost", method = RequestMethod.POST)
    public String createPost(HttpServletRequest req) {
        boolean approved;
        boolean visible;
        String content = req.getParameter("postContent");
        String title = req.getParameter("title");
        String username = req.getParameter("username");
        if (username.equals("admin")) {
            approved = true;
            visible = true;
        } else {
            approved = false;
            visible = false;
        }
        String[] tags = req.getParameterValues("new-tags");
        List<Tag> tagList = new ArrayList<>();
        if(tags != null){
            for (String s : tags) {
                if(!s.trim().isEmpty()){
                    tagList.add(new Tag(s));
                }
            }
        }
        String startDate = req.getParameter("startDate");
        if(startDate.isEmpty()){
            startDate = LocalDate.now().toString();
        }
        String endDate = req.getParameter("endDate");
        if(endDate.isEmpty()){
            endDate = "2999-12-12";
        }

        Post p = new Post(title, username, content, approved, visible, LocalDate.parse(startDate), LocalDate.parse(endDate), tagList);
        dao.addPost(p);

        return "redirect:mainAdmin";
    }

    @RequestMapping(value = "/page/{id}", method = RequestMethod.GET)
    public String getPage(@PathVariable("id") int id, Model model) {
        // Retrieve the Post associated with the given id and return it
        model.addAttribute("page_id", id);
        String title = dao.getPageById(id).getTitle();
        model.addAttribute("title", dao.getPageById(id).getTitle());
        model.addAttribute("content", dao.getPageById(id).getContent());

        return "pages";

    }

    @RequestMapping(value = "/page/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePage(@PathVariable("id") int id) {
        dao.removePage(id);
    }

    /**
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/addNewPage", method = RequestMethod.POST)
    public String addNewPage(HttpServletRequest req) {
        String content = req.getParameter("postContent");
        String title = req.getParameter("title");

        Page p = new Page();
        p.setContent(content);
        p.setTitle(title);

        dao.addPage(p);

        return "redirect:mainAdmin";
    }

    public Post postThings(@RequestBody Wrapper wrapper) {
        Post mypost = wrapper.post;
        Tag mytag = wrapper.tag;
        dao.addPost(mypost);
        return mypost;
    }
// @RequestMapping(value="/postTag", method = RequestMethod.POST)
// @ResponseStatus(HttpStatus.CREATED)   
// @ResponseBody
// public Tag createTag(@RequestBody Tag tag){
//        dao.addTag(tag, thepost);
//        return tag;
//    }

    @RequestMapping(value = "/post/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putPost(@PathVariable("id") int id, @RequestBody Post post) {
        // set the value of the PathVariable id on the incoming Post object
        // to ensure that a) the Post id is set on the object and b) that
        // the value of the PathVariable id and the Post object id are the
        // same.
        post.setPostID(id);
        // update the post
        dao.updatePost(post);
    }

}
