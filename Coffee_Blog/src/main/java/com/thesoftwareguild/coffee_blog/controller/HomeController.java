package com.thesoftwareguild.coffee_blog.controller;

import com.thesoftwareguild.coffee_blog.dao.PostDAO;
import com.thesoftwareguild.coffee_blog.dto.Page;
import com.thesoftwareguild.coffee_blog.dto.Post;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    private PostDAO dao;

    @Inject
    public HomeController(PostDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        dao.updateVisibility();
        return "home";
    }
    @RequestMapping(value = {"/addPost"}, method = RequestMethod.GET)
    public String displayAddPost() {
        return "addPost";
    }
    @RequestMapping(value = {"/addPage"}, method = RequestMethod.GET)
    public String displayAddPaget() {
        return "addPage";
    }
    @RequestMapping(value = {"/mainAjaxPage"}, method = RequestMethod.GET)
    public String displayMainAjaxPage() {
        return "mainAjaxPage";
    }

    @RequestMapping(value = "/visiblePosts", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getAllPosts() {
        return dao.getVisiblePosts();
    }
    
    @RequestMapping(value = "/filterPostsByTag/{tag}", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> filterPosts(@PathVariable("tag") int tagId){
        return dao.filterPosts(tagId);
    }
    
    @RequestMapping(value = "/pages", method = RequestMethod.GET)
    @ResponseBody
    public List<Page> getAllPages() {
        return dao.getAllPages();
    }
   
    

}