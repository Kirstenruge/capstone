/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.controller;

import com.thesoftwareguild.coffee_blog.dao.UserDao;
import com.thesoftwareguild.coffee_blog.dto.User;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class UserController {

    private UserDao dao;
// #1 - PasswordEncoder interface
    private PasswordEncoder encoder;
// #2 - Inject a PasswordEncoder

    @Inject
    public UserController(UserDao dao, PasswordEncoder encoder) {
        this.dao = dao;
        this.encoder = encoder;
    }

    @RequestMapping(value = "/displayUserList", method = RequestMethod.GET)
    public String displayUserList(Map<String, Object> model) {
        List users = dao.getAllUsers();
        model.put("users", users);
        return "displayUserList";
    }
    
    @RequestMapping(value = "/addUserPage", method = RequestMethod.GET)
    public String displayUserForm(Map<String, Object> model) {
        return "addUserForm";
    }
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUser(HttpServletRequest req) {
        User newUser = new User();
        newUser.setUsername(req.getParameter("username"));
// #3 - Hash the password and then set it on the User object before saving it
        String clearPw = req.getParameter("password");
        String hashPw = encoder.encode(clearPw);
        newUser.setPassword(hashPw);
        if (null != req.getParameter("isAdmin")) {
            newUser.addAuthority("ROLE_ADMIN");
        } else {
            newUser.addAuthority("ROLE_MARKET");
        }
        dao.addUser(newUser);
        return "redirect:displayUserList";
    }
    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deletUser(@RequestParam("username") String username) {
        dao.deleteUser(username);
        return "redirect:displayUserList";
    }
    
    @RequestMapping(value="/changePassword", method = RequestMethod.POST)
    public String displayUpdatePassword(HttpServletRequest req){
        String username = req.getParameter("username");
        String passwordClear = req.getParameter("new-password");
        String hashPw = encoder.encode(passwordClear);
        dao.changePassword(username, hashPw);
        return "redirect:displayUserList";
    }
//    
}
