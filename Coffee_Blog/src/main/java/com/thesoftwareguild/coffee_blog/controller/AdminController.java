/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.controller;

import com.thesoftwareguild.coffee_blog.dao.PostDAO;
import com.thesoftwareguild.coffee_blog.dto.Page;
import com.thesoftwareguild.coffee_blog.dto.Post;
import com.thesoftwareguild.coffee_blog.dto.Tag;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class AdminController {

    private PostDAO dao;

    @Inject
    public AdminController(PostDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/allPosts", method = RequestMethod.GET)
    @ResponseBody
    public List<Post> displayAllPosts() {
        return dao.getAllPosts();
    }

    @RequestMapping(value = "approvePosts", method = RequestMethod.GET)
    public String displayApprovePosts() {
        return "approvePosts";
    }

    @RequestMapping(value = "editPost/{id}", method = RequestMethod.GET)
    public String displayEditPost(@PathVariable("id") int id, Model model) {
        Post p = dao.getPostById(id);
        model.addAttribute("post", p);
        return "editPost";
    }

    @RequestMapping(value = "editPages", method = RequestMethod.GET)
    public String displayEditPages() {
        return "editPages";
    }

    @RequestMapping(value = "editPage/{id}", method = RequestMethod.GET)
    public String displayEditPage(@PathVariable("id") int id, Model model) {
        Page p = dao.getPageById(id);
        model.addAttribute("page", p);
        return "editPage";
    }

    @RequestMapping(value = "deletePage/{id}", method = RequestMethod.GET)
    public String deletePage(@PathVariable("id") int id) {
        dao.removePage(id);
        return "redirect:../editPages";
    }

    @RequestMapping(value = "deletePost/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable("id") int id) {
        dao.removePost(id);
        return "redirect:../mainAdmin";
    }

    @RequestMapping(value = {"/unapprovedPosts"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Post> getUnapprovedPosts() {
        return dao.getUnapprovedPosts();
    }

    @RequestMapping(value = "/editPostSubmit", method = RequestMethod.POST)
    public String editPostSubmit(HttpServletRequest req) {
        boolean approved = Boolean.valueOf(req.getParameter("approved"));
        boolean visible = Boolean.valueOf(req.getParameter("visible"));
        String content = req.getParameter("content");
        String title = req.getParameter("title");
        String username = req.getParameter("username");
        String[] tags = req.getParameterValues("new-tags");
        List<Tag> tagList = new ArrayList<>();
        if (tags != null) {
            for (String s : tags) {
                if (!s.trim().isEmpty()) {
                    tagList.add(new Tag(s));
                }
            }
        }
        String startDate = req.getParameter("postDateString");
        if (startDate == null || startDate.isEmpty()) {
            startDate = LocalDate.now().toString();
        }
        String endDate = req.getParameter("expirationDateString");
        if (endDate == null || endDate.isEmpty()) {
            endDate = "2999-12-12";
        }

        Post p = new Post(title, username, content, approved, visible, LocalDate.parse(startDate), LocalDate.parse(endDate), tagList);
        p.setCreationDate(LocalDate.parse(req.getParameter("creationDateString")));
        p.setPostID(Integer.valueOf(req.getParameter("postID")));
        dao.updatePost(p);

        return "redirect:mainAdmin";
    }

    @RequestMapping(value = "/editPageSubmit", method = RequestMethod.POST)
    public String editPageSubmit(@ModelAttribute("page") Page page, BindingResult result) {
        if (result.hasErrors()) {
            return "editPage";
        }
        dao.updatePage(page);
        return "redirect:editPages";
    }

    @RequestMapping(value = "approvePost/{id}", method = RequestMethod.GET)
    public String approvePost(@PathVariable("id") int id) {
        Post p = dao.getPostById(id);
        p.setApproved(true);
        dao.updatePost(p);
        return "redirect:../mainAdmin";
    }

    @RequestMapping(value = "addTag/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putTag(@PathVariable("id") int postId, @RequestBody Tag tag) {
        tag.setPostID(postId);
        dao.addTag(tag);
    }
    
    @RequestMapping(value = "getTags/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Tag> getTags(@PathVariable("id") int tagId){
        return dao.getTagsById(tagId);
    };
}
