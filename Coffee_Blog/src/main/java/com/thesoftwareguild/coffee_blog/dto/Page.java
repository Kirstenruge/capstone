/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dto;

/**
 *
 * @author apprentice
 */
public class Page {
    private String content;
    private int pageID;
    private String title;

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the pageID
     */
    public int getPageID() {
        return pageID;
    }

    /**
     * @param pageID the pageID to set
     */
    public void setPageID(int pageID) {
        this.pageID = pageID;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
