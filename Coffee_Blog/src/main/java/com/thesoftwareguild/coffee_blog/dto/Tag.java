/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dto;

/**
 *
 * @author apprentice
 */
public class Tag {
    private String tag;
    private int tagID;
    private int postID;

    public Tag(){};
    
    public Tag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public int getTagID() {
        return tagID;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setTagID(int tagID) {
        this.tagID = tagID;
    }

    /**
     * @return the postID
     */
    public int getPostID() {
        return postID;
    }

    /**
     * @param postID the postID to set
     */
    public void setPostID(int postID) {
        this.postID = postID;
    }
    
}
