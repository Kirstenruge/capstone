/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class User {
    private String username;
    private String password;
    private List <String> authorities = new ArrayList<>();

    public User(){};
    
    public User(String username, String password, List <String> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List <String> getAuthorities(){
        return authorities;
    }
            
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }
    
    public void addAuthority(String authority) {
        authorities.add(authority);
    }
    
}
