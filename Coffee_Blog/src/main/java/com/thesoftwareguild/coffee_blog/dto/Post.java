/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.coffee_blog.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class Post {

    private int postID;
    private String title;
    private LocalDate creationDate;
    private String username;
    private String content;
    private boolean approved;
    private boolean visible;
    private LocalDate postDate;
    private LocalDate expirationDate;
    private List<Tag> tags;

    public Post() {
    };
    public Post(String title, String username, String content, boolean approved, boolean visible, LocalDate postDate, LocalDate expirationDate, List<Tag> tags) {
        creationDate = LocalDate.now();
        this.title = title;
        this.username = username;
        this.content = content;
        this.approved = approved;
        this.visible = visible;
        this.postDate = postDate;
        this.expirationDate = expirationDate;
        this.tags = tags;
    }

    public void setPostDate(LocalDate postDate) {
        this.postDate = postDate;
    }
   

    public LocalDate getPostDate() {
        return postDate;
    }

    public int getPostID() {
        return postID;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public String getUsername() {
        return username;
    }

    public String getContent() {
        return content;
    }

    public boolean isApproved() {
        return approved;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Post other = (Post) obj;
        if (this.approved != other.approved) {
            return false;
        }
        if (this.visible != other.visible) {
            return false;
        }
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        if (!Objects.equals(this.creationDate, other.creationDate)) {
            return false;
        }
        if (!Objects.equals(this.postDate, other.postDate)) {
            return false;
        }
        if (!Objects.equals(this.expirationDate, other.expirationDate)) {
            return false;
        }
        return true;
    }

    /**
     * @return the creationDateString
     */
    public String getCreationDateString() {
        return creationDate.toString();
    }

    /**
     * @return the postDateString
     */
    public String getPostDateString() {
        return postDate.toString();
    }

    /**
     * @return the expirationDateString
     */
    public String getExpirationDateString() {
        return expirationDate.toString();
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }


}
