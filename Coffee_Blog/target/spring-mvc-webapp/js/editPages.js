/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    loadPages();

});

function loadPages() {
    $.ajax({
        url: "pages"
    }).success(function (data, status) {
        fillLinks(data, status);
    });
}

function fillLinks(pageList, status) {

    var $links = $("#page-links");
    $.each(pageList, function (index, page) {
        str = page.content;

        title = page.title;
        pageid = page.pageID;
        html = $.parseHTML(str),
                nodeNames = [];

// Append the parsed HTML

        $links.append('<p>');
        $links.append('<a href="' + "page/" + pageid + '">' + title + '</a>');
        $links.append(" | ");
        $links.append('<a href="' + "editPage/" + pageid + '">Edit</a>');
        $links.append(" | ");
        $links.append('<a href="' + "deletePage/" + pageid + '">Delete</a>');


// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
}