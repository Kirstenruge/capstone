/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var wordArray = [];
$(document).ready(function () {
    loadPosts();
    loadPages();
    $(document.body).on("click", ".tag-filter", function (event) {
        event.preventDefault();
        var tagId = $(this).attr('name');
        var url = 'filterPostsByTag/' + tagId;
        $.ajax({
            url: url
        }).success(function (data, status) {
            clearPostsDiv();
            fillLog(data, status);
        });
    });
});
function loadPages() {
    $.ajax({
        url: "pages"
    }).success(function (data, status) {
        fillLinks(data, status);
    });
}
function loadPosts() {
    $.ajax({
        url: "visiblePosts"
    }).success(function (data, status) {
        fillLog(data, status);
    });
}

function clearPagesDiv() {
    $('#links').empty();
}
function clearPostsDiv() {
    $('#log').empty();
}

function fillLog(postList, status) {
    var $log = $("#log");
    $.each(postList, function (index, post) {
        str = post.content;
        date = post.creationDateString;
        postDate = post.postDateString;
        title = post.title;
        author = post.username;
        html = $.parseHTML(str),
                nodeNames = [];
        tags = post.tags;
        var visibleDate;
        if(postDate === "1900-01-01"){
            visibleDate = date;
        }else{
            visibleDate = postDate;
        }
// Append the parsed HTML
        $log.append($('<div class="well">').append(title));
        $log.append($('</div>'));
        $log.append($('<div>')).append('<strong>Posted on: </strong>' + visibleDate + '<br>' + "<strong>By: " + '</strong>' + author);
        $log.append($('<br>'));
        $log.append($('<br>'));
        $log.append($('<div>').append(html));
        $.each(tags, function (index, tag) {
            $log.append($('<a>').attr("name", tag.tagID).attr("href", "#").addClass("tag-filter").append($('<button class="btn btn-small btn-danger">').text(tag.tag))).append(" ");
            if(wordArray.length === 0){
                wordArray.push({tag: tag.tag, rel: 1, tagID: tag.tagID});
            }else{
                var found = false;
                for(var i = 0; i < wordArray.length; i++){
                    if(wordArray[i].tag === tag.tag){
                        wordArray[i].rel++;
                        found = true;
                    }
                }
                if (!found){
                    wordArray.push({tag: tag.tag, rel: 1, tagID: tag.tagID});
                }
            }
        });
        $log.append($('<hr>'));



// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
    populateWordTable();
}
function fillLinks(pageList, status) {

    var $links = $("#links");
    $.each(pageList, function (index, page) {
        str = page.content;

        title = page.title;
        pageid = page.pageID;
        html = $.parseHTML(str),
                nodeNames = [];

// Append the parsed HTML

        $("div#links").append('<p>');
        var ul = $("div#links");

        ul.append('<a href="' + "page/" + pageid + '">' + title + '</a>');



// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
}
function populateWordTable(){
    $('#tag-links-div').empty();
    for(var i = 0; i < wordArray.length; i++){
        var tag = wordArray[i];
        $('#tag-links-div').append(($('<a>').attr("name", tag.tagID).attr("href", "#").addClass("tag-filter").append($('<button class="btn btn-small btn-danger">').text(tag.tag)))).append(" ");
    }
}
