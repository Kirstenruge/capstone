/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadPages();
    
});
function loadPages() {
    $.ajax({
        url: "pages"
    }).success(function (data, status) {
        fillLog(data, status);
    });
}


function clearPagesDiv() {
    $('#pagesdiv').empty();
}

function fillLog(pageList, status) {
    var $log = $("#log");
    $.each(pageList, function (index, page) {
        str = page.content;
        
        title = page.title;
        html = $.parseHTML(str),
                nodeNames = [];

// Append the parsed HTML
        $log.append(date);
        $log.append($('<br>'));
        $log.append(title);
        $log.append($('<br>'));
        $log.append(html);
        $log.append($('<br>'));
        $log.append($('<hr>'));


// Gather the parsed HTML's node names
        $.each(html, function (i, el) {
            nodeNames[ i ] = "<li>" + el.nodeName + "</li>";
        });
    });
}


function fillPagesDiv(pageList, status) {
    clearPagesDiv();
    var cTable = $('#contents');

    $.each(pageList, function (index, page) {

        cTable.append($('<tr>')
                .append($('<td>')

                        .append($('<a>')
                                .attr({
                                    'data-page-id': page.pageID,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(page.pageID + ' ')
                                ) // ends the <a> tag
                        ) // ends the <td> tag for the dvd name
                .append($('<td>').text(page.content))
                .append($('<hr>'))
                .append($('<br>'))


                )       // ends the <td> tag for Delete
                ;

        ;
    }); // ends the 'each' function
}
