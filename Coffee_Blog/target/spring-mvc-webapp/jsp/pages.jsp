<%-- 
    Document   : pages
    Created on : Dec 2, 2016, 10:08:41 AM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
      <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="logo" href="${pageContext.request.contextPath}/img/coffeecup.jpg">
      
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="container-fluid">
                <nav class="navbar navbar-masthead navbar-fixed-top navbar-custom">
                    <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="${pageContext.request.contextPath}/home"><img src="../img/coffeecup.jpg" alt="Logo"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>
                                <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                <li><a href="${pageContext.request.contextPath}/mainAdmin">Admin Portal</a></li>
                                </sec:authorize>
                        </ul>
                        <sec:authorize access="!hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/login">Log In</a></li>
                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a></li>
                            </ul>
                        </sec:authorize>
                    </div>  </div>
                </nav>
                                <div class="row">
                    <div class="row">
                         <div class="banner">
                              
                            </div>
                        <br>
               
                        <div class="col-sm-7 col-sm-offset-1 blog-main">

                            <center>
                                <h1>${title}</h1><hr class="custom"></center>
                            
                            <div> ${content} </div>
                           
                               
                                
                               
                               
                             
                                
                           
                       

                           
                           

                        </div>
                         <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
                            <div class="sidebar-module sidebar-module-inset">
                                <center>
                                    <h2>Beanz Coffee</h2>
                                    <p> 600 Washington Ave N. <br>
                                        Minneapolis, MN 55404 </p>
                                    <div class="iframe-container">
                                        <iframe width="225" height="200" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJlXRGEYkys1IRgVhbnldvzxk&key=AIzaSyBoCBluUdCoWS4hctO3rgKK_kwoRGrIv84" allowfullscreen></iframe>
                                    </div>
                                    <div class="sidebar-module">
                                        <div id="links"> </div>
                                    </div>
                                    <div class="sidebar-module">
                                        <h2>Tags</h2>
                                        <div id="tag-links-div">
                                        </div>
                                    </div>
                                    <div class="sidebar-module">
                                        <h2>Links</h2>
                                        <ol class="list-unstyled">
                                           
                                            <li> <a href="https://twitter.com/intent/tweet?button_hashtag=beanzcoffee" class="twitter-hashtag-button" data-show-count="false">Tweet #beanzcoffee</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></li>
                                            <div id="fb-root"></div>
                                            <script>(function (d, s, id) {
                                                    var js, fjs = d.getElementsByTagName(s)[0];
                                                    if (d.getElementById(id))
                                                        return;
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }(document, 'script', 'facebook-jssdk'));</script>
                                            <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="box_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
                                        </ol>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-4">
            <a class="twitter-timeline" href="https://twitter.com/beanzcoffeemn">Tweets by beanzcoffeemn</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>          
                                           
                                   
                                        </div> 
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


        <script src="${pageContext.request.contextPath}/js/links.js"></script>
    </body>
</html>

