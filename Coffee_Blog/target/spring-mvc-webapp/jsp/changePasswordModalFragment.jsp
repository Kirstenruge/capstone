<form class="form-horizontal" role="form" method="POST" action="/changePassword">
    <div class="modal fade" id="change-password-modal" tabindex="-1" role="dialog"
         aria-labelledby="pw-modal-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="pw-modal-label">Change Password</h4>
                </div>
                <div class="modal-body">
                    <h3>What Would You Like to Change Password To?</h3>
                    <div class="form-group">
                        <label for="new-tag" id="tag-label" class="col-md-4 control-label hidden">
                            Change Password
                        </label>
                        <div class="col-md-offset-1 col-md-11" id="change-pw-div">
                            <input type="password" id="new-password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-11">
                            <button type="submit" id="change-password-button" class="btn btn-default"
                                    data-dismiss="modal">
                                Submit
                            </button>
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">
                                Cancel
                            </button>
                            <input type="hidden" id="username">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</form>

