<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="logo" href="${pageContext.request.contextPath}/img/coffeecup.jpg">
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-masthead navbar-fixed-top navbar-custom">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="home"><img src="img/coffeecup.jpg" alt="Logo"></a>
                    
                    </div>
                  
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>
                            <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                <li class="active"><a href="${pageContext.request.contextPath}/mainAdmin">Admin Portal</a></li>
                            </sec:authorize>
                        </ul>
                        <sec:authorize access="!hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/login">Log In</a></li>
                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a></li>
                            </ul>
                        </sec:authorize>
                    </div>  </div>
            </nav>
            <div class="row">
                
                <h2 class="text-center"> Add New User </h2><hr class="custom">
                <div class="col-sm-7 col-sm-offset-1">
                    <h2>Add User Form</h2>
                    <form method="POST" action="addUser">
                        Username: <input type="text" name="username"/><br/><br/>
                        Password: <input type="password" name="password"/><br/><br/>
                        Admin User? <input type="checkbox" name="isAdmin" value="yes"/><br/><br/>
                        <input class="btn" type="submit" value="Add User"/><br/>
                    </form>
                </div>
                 <div class="col-sm-3 col-sm-offset-1 blog-sidebar text-center">
                    What would you like to do?<br>
                    <a href="${pageContext.request.contextPath}/addPost"> Add a Blog Post</a><br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a id="approve_posts_link" href="${pageContext.request.contextPath}/approvePosts">Approve Posts</a><br>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">    
                        <a href="${pageContext.request.contextPath}/addPage"> Add a New Page</a><br>
                    </sec:authorize>
                    <sec:authorize  access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.request.contextPath}/editPages">Edit Pages</a><br>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.request.contextPath}/displayUserList">Edit Users</a><br>
                    </sec:authorize>
                </div>
            </div>
                    <div class="row">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br><br><br>
                          <hr class="custom">
                    </div>
        </div>
    </body>
</html>