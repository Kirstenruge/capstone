<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Beanz Coffee</title>
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <link rel="logo" href="${pageContext.request.contextPath}/img/coffeecup.jpg">
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-masthead navbar-fixed-top navbar-custom">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-masthead-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="home"><img src="img/coffeecup.jpg" alt="Logo"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse bs-example-masthead-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="${pageContext.request.contextPath}/home">Home <span class="sr-only">(current)</span></a></li>
                                <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                                <li class="active"><a href="${pageContext.request.contextPath}/mainAdmin">Admin Portal</a></li>
                                </sec:authorize>
                        </ul>
                        <sec:authorize access="!hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/login">Log In</a></li>
                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ADMIN,ROLE_MARKET')">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/j_spring_security_logout">Log Out</a></li>
                            </ul>
                        </sec:authorize>
                    </div>  </div>
            </nav>
            <div class="row"> 
                <h2 class="text-center"> Welcome, <sec:authentication property="principal.username"/>! </h2><hr class="custom">
                <div class="col-sm-7 col-sm-offset-1"id="users-div">
                    <div class="text-center">
                        <h1>Users</h1>
                        <a href="addUserPage">Add a User</a><br/><br/>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>Username</th>
                            <th>Delete</th>
                            <th>Change Password</th>
                        </tr>
                        <tbody>
                            <c:forEach var="user" items="${users}">
                                <tr>
                                    <td><c:out value="${user.username}"/></td>
                                    <c:if test="${user.username != 'admin'}">
                                        <td><a href="deleteUser?username=${user.username}">Delete</a></td>
                                    </c:if>
                                    <c:if test="${user.username == 'admin'}">
                                        <td>Cannot Delete admin</td>
                                    </c:if>
                                    <td><button class="btn" disabled data-toggle="modal" data-target="#change-password-modal" data-username="${user.username}"><span class="glyphicon glyphicon-edit"></span></button></td>
                                </tr>
                            </c:forEach>
                    </table>
                    <br>
                    <br>
                    <br><br>
                    <br>
                
                  
                </div>
                <div class="col-sm-3 col-sm-offset-1 blog-sidebar text-center">
                    What would you like to do?<br>
                    <a href="${pageContext.request.contextPath}/addPost"> Add a Blog Post</a><br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a id="approve_posts_link" href="${pageContext.request.contextPath}/approvePosts">Approve Posts</a><br>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">    
                        <a href="${pageContext.request.contextPath}/addPage"> Add a New Page</a><br>
                    </sec:authorize>
                    <sec:authorize  access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.request.contextPath}/editPages">Edit Pages</a><br>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.request.contextPath}/displayUserList">Edit Users</a><br>
                    </sec:authorize>
                </div>
            </div>
                    <div class="row">
                          <hr class="custom">
                    </div>
        </div>
        <!--<//%@include file="changePasswordModalFragment.jsp"%>-->
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <!--<script>
            $('#changePasswordModal').on('show.bs.modal', function(e){
               var username = e.relatedTarget.dataset.username;
               $('#username').val(username);
            });
        </script>-->
    </body>
</html>