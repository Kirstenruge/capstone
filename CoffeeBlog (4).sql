-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 08, 2016 at 11:56 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `CoffeeBlog`
--

-- --------------------------------------------------------

--
-- Table structure for table `Authorities`
--

CREATE TABLE `Authorities` (
  `username` varchar(20) NOT NULL,
  `authority` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Authorities`
--

INSERT INTO `Authorities` (`username`, `authority`) VALUES
('Admin', 'ROLE_ADMIN'),
('Marketing', 'ROLE_MARKET');

-- --------------------------------------------------------

--
-- Table structure for table `Pages`
--

CREATE TABLE `Pages` (
  `page_id` tinyint(11) NOT NULL,
  `content` text NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Pages`
--

INSERT INTO `Pages` (`page_id`, `content`, `title`) VALUES
(4, '<p style="padding-left: 30px;">Beanz coffee was started as just an idea in the brains of three gorgeous and majestic software engineering students in late 2016. Back then, one had to trek all the way up the escalator two floors up to the Starbucks in the IDS center for a mediocre-yet-overpriced coffee. All this was about to change in a big way.</p>\r\n<p style="padding-left: 30px;">Beanz Coffee grew to be an impressive innovator of overpriced, underflavored coffee related beverages and soon took over the majority of the caffeine market within a half block radius of Globe University.</p>\r\n<p style="padding-left: 30px;">To this day, we still show the same dedication as we did when we started this company, three weeks ago.&nbsp;</p>\r\n<p style="padding-left: 30px;">To thine own self be true.</p>\r\n<p style="padding-left: 30px;">&nbsp;</p>', 'About'),
(6, '<p style="text-align: center;"><strong>Winter Hours:</strong></p>\r\n<p style="text-align: center;"><strong>M-F: 5AM - 10PM</strong></p>\r\n<p style="text-align: center;"><strong>Sat and Sun: 9AM - 9PM</strong></p>', 'Hours');

-- --------------------------------------------------------

--
-- Table structure for table `Posts`
--

CREATE TABLE `Posts` (
  `post_id` int(11) NOT NULL,
  `creation_date` date NOT NULL,
  `username` varchar(20) NOT NULL,
  `content` text NOT NULL,
  `approved` bit(1) NOT NULL,
  `post_date` date NOT NULL,
  `expiration_date` date NOT NULL,
  `visible` bit(1) DEFAULT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Posts`
--

INSERT INTO `Posts` (`post_id`, `creation_date`, `username`, `content`, `approved`, `post_date`, `expiration_date`, `visible`, `title`) VALUES
(17, '2016-12-06', 'marketing', '<p style="text-align: center;"><strong>For a limited time only!&nbsp;</strong></p>\r\n<p style="text-align: center;"><strong><img src="http://magicbulletblog.com/wp-content/uploads/2013/12/peppermint-mocha1.jpg" width="400" height="387" /></strong></p>', b'1', '2016-12-06', '2016-12-31', b'1', 'Candycane lattes are here!'),
(18, '2016-12-07', 'admin', '<p>The coffee tree is a tropical evergreen shrub (genus Coffea) and grows between the Tropics of Cancer and Capricorn. The two most commercially important species grown are varieties of Coffea arabica (Arabicas) and Coffea canephora (Robustas).</p>\r\n<p>The average Arabica plant is a large bush with dark-green oval leaves. The fruits, or cherries, are rounded and mature in 7 to 9 months; they usually contain two flat seeds, the coffee beans. When only one bean develops it is called a peaberry.</p>\r\n<p>Robusta is a robust shrub or small tree that grows up to 10 metres high. The fruits are rounded and take up to 11 months to mature; the seeds are oval in shape and smaller than Arabica seeds.</p>\r\n<p>Ideal average temperatures range between 15 to 24&ordm;C for Arabica coffee and 24 to 30&ordm;C for Robusta, which can flourish in hotter, harsher conditions. Coffee needs an annual rainfall of 1500 to 3000 mm, with Arabica needing less than other species. Whereas Robusta coffee can be grown between sea-level and about 800 metres, Arabica does best at higher altitudes and is often grown in hilly areas.</p>\r\n<p><strong>Harvesting</strong></p>\r\n<p>As coffee is often grown in mountainous areas, widespread use of mechanical harvesters is not possible and the ripe coffee cherries are usually picked by hand. The main exception is Brazil, where the relatively flat landscape and immense size of the coffee fields allow for machinery use.</p>\r\n<p>Coffee trees yield an average of 2 to 4 kilos of cherries and a good picker can harvest 45 to 90 kilos of coffee cherry per day; this will produce nine to 18 kilos of coffee beans.</p>\r\n<p>Coffee is harvested in one of two ways:</p>\r\n<p>Strip Picked &ndash; all the cherries are stripped off of the branch at one time, either by machine or by hand.</p>\r\n<p>Selectively Picked &ndash; only the ripe cherries are harvested and they are picked by hand.</p>\r\n<p>Pickers check the trees every 8 to 10 days and individually pick only the fully ripe cherries. This method is labour intensive and more costly. Selective picking is primarily used for the finer Arabica beans.</p>', b'1', '2016-12-07', '2999-12-12', b'1', 'Do you know where your coffee beans come from?'),
(19, '2016-12-08', 'Admin', '<p>Coffee is much more than just a delicious &ldquo;Pick me up&rdquo; in the morning; it&rsquo;s known to be loaded with active compounds and vitamins that have a variety of health-promoting characteristics. Drinking coffee has been associated with lowering the chances of developing Parkinson&rsquo;s disease, certain types of cancers and even reducing the risk of clogging arteries, which can lead to heart attacks.</p>\r\n<p><img src="http://shivaschicken.ca/uploads/thumb/Fresh%20Brewed%20Coffee%20Candle_1385615866_1331866552.jpg" alt="" width="300" height="188" /></p>', b'1', '2016-12-08', '2999-12-12', b'1', 'Coffee is good for you!'),
(20, '2016-12-08', 'Marketing', '<p>post</p>', b'1', '2016-12-08', '2016-12-29', b'1', 'stuff');

-- --------------------------------------------------------

--
-- Table structure for table `staticPages`
--

CREATE TABLE `staticPages` (
  `URL` text NOT NULL,
  `content` text NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Tags`
--

CREATE TABLE `Tags` (
  `tag` varchar(50) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tags`
--

INSERT INTO `Tags` (`tag`, `post_id`, `tag_id`) VALUES
('#coffeeoftheday', 17, 30),
('#merrychristmas', 17, 31),
('#flavors', 17, 32),
('#history', 18, 33),
('#themoreyouknow', 18, 34),
('#themoreyouknow', 19, 37),
('#health', 19, 38),
('#blessed', 20, 40);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `username` varchar(20) NOT NULL,
  `password` char(60) NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`username`, `password`, `enabled`) VALUES
('Admin', '$2a$10$Y7Li/M3YIXHAY0wUhFeROevpRDqRNWnevhNHBIjaKZaN3KQuWnKFu', 1),
('Marketing', '$2a$10$OdFzoVlNfLdIyDHeLfBla.DZexUTU6OACYhs/CVc4TMQpco82rP46', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Authorities`
--
ALTER TABLE `Authorities`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `Pages`
--
ALTER TABLE `Pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `Posts`
--
ALTER TABLE `Posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `staticPages`
--
ALTER TABLE `staticPages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `Tags`
--
ALTER TABLE `Tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Pages`
--
ALTER TABLE `Pages`
  MODIFY `page_id` tinyint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `Posts`
--
ALTER TABLE `Posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `staticPages`
--
ALTER TABLE `staticPages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Tags`
--
ALTER TABLE `Tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
